package com.example.currency.presentation

import com.example.core.domain.usecase.ObservableUseCase
import com.example.core.presentation.BasePresenter
import com.example.currency.domain.model.ExchangeRates
import com.example.currency.domain.usecase.UpdateExchangeRatesUseCase
import com.example.currency.presentation.model.ExchangeRatesViewModelMapper
import javax.inject.Inject


class ExchangeRatesPresenter @Inject constructor(val view: ExchangeRatesContract.View,
                                                 private val updateExchangeRatesUseCase: UpdateExchangeRatesUseCase,
                                                 private val exchangeRatesViewModelMapper: ExchangeRatesViewModelMapper
)
    : BasePresenter(), ExchangeRatesContract.Presenter {


    private var baseCurrency: String? = null

    companion object {
        const val UPDATE_USE_CASE_TAG = "UPDATE"
    }

    override fun init() {
        super.init()
        view.initView()
    }

    override fun start() {
        super.start()
        startListeningForExchangeRates()
    }

    private fun startListeningForExchangeRates(){
        updateExchangeRatesUseCase.execute(GetLatestExchangeRatesSubscriber(), baseCurrency).addPersistDisposable(
            UPDATE_USE_CASE_TAG)
    }

    override fun onCurrencyClicked(currency: String) {
        disposePersistDisposableByTag(UPDATE_USE_CASE_TAG)
        baseCurrency = currency
        startListeningForExchangeRates()
    }

    override fun onCurrencyStateSaved(): String? {
        return baseCurrency
    }

    override fun onCurrencyStateRestored(currency: String?) {
        baseCurrency = currency
    }

    private inner class GetLatestExchangeRatesSubscriber : ObservableUseCase.Subscriber<ExchangeRates>() {
        override fun onSuccess(result: ExchangeRates) {
            val data = exchangeRatesViewModelMapper.mapToView(result)
            view.showExchangeRates(data)
        }

        override fun onError(exp: Throwable) {}
    }
}
