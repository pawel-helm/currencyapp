package com.example.currency.presentation

import com.example.core.presentation.BaseContract
import com.example.currency.presentation.model.ExchangeRateViewModel


interface ExchangeRatesContract {


    interface View : BaseContract.View {
        fun initView()
        fun showExchangeRates(data: List<ExchangeRateViewModel>)
    }

    interface Presenter : BaseContract.Presenter {
        fun onCurrencyClicked(currency: String)
        fun onCurrencyStateSaved(): String?
        fun onCurrencyStateRestored(currency: String?)
    }

}