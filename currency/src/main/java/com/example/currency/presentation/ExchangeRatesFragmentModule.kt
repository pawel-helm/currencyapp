package com.example.currency.presentation

import com.example.currency.domain.usecase.UpdateExchangeRatesUseCase
import com.example.currency.presentation.model.ExchangeRatesViewModelMapper
import dagger.Module
import dagger.Provides


@Module
class ExchangeRatesFragmentModule {

    @Provides
    fun provideExchangeRatesView(ExchangeRatesFragment: ExchangeRatesFragment): ExchangeRatesContract.View =
        ExchangeRatesFragment

    @Provides
    fun provideExchangeRatesPresenter(
        view: ExchangeRatesContract.View,
        updateExchangeRatesUseCase: UpdateExchangeRatesUseCase,
        exchaneRatesViewModelMapper: ExchangeRatesViewModelMapper
    ): ExchangeRatesContract.Presenter =
        ExchangeRatesPresenter(view, updateExchangeRatesUseCase, exchaneRatesViewModelMapper)
}