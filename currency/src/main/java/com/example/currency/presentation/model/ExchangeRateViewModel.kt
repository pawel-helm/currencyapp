package com.example.currency.presentation.model

import com.example.currency.R
import com.example.currency.domain.model.CurrencyConstants
import java.math.BigDecimal

sealed class ExchangeRateViewModel (
    val shortName: String,
    var exchangeRate: BigDecimal,
    var multipliedValue: BigDecimal,
    val fullNameResourceId: Int,
    val flagResourceId: Int
){

    class AUD(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.AUD, value, value, R.string.AUD, R.drawable.flag_australia)
    class BGN(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.BGN, value, value, R.string.BGN, R.drawable.flag_bulgaria)
    class BRL(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.BRL, value, value, R.string.BRL, R.drawable.flag_brazil)
    class CAD(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.CAD, value, value, R.string.CAD, R.drawable.flag_canada)
    class CHF(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.CHF, value, value, R.string.CHF, R.drawable.flag_switzerland)
    class CNY(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.CNY, value, value, R.string.CNY, R.drawable.flag_china)
    class CZK(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.CZK, value, value, R.string.CZK, R.drawable.flag_czech)
    class DKK(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.DKK, value, value, R.string.DKK, R.drawable.flag_denmark)
    class EUR(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.EUR, value, value, R.string.EUR, R.drawable.flag_euro)
    class GBP(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.GBP, value, value, R.string.GBP, R.drawable.flag_great_britain)
    class HKD(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.HKD, value, value, R.string.HKD, R.drawable.flag_hong_kong)
    class HRK(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.HRK, value, value, R.string.HRK, R.drawable.flag_croatia)
    class HUF(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.HUF, value, value, R.string.HUF, R.drawable.flag_hungary)
    class IDR(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.IDR, value, value, R.string.IDR, R.drawable.flag_indonesia)
    class ILS(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.ILS, value, value, R.string.ILS, R.drawable.flag_israel)
    class INR(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.INR, value, value, R.string.INR, R.drawable.flag_india)
    class ISK(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.ISK, value, value, R.string.ISK, R.drawable.flag_iceland)
    class JPY(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.JPY, value, value, R.string.JPY, R.drawable.flag_japan)
    class KRW(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.KRW, value, value, R.string.KRW, R.drawable.flag_korea)
    class MXN(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.MXN, value, value, R.string.MXN, R.drawable.flag_mexico)
    class MYR(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.MYR, value, value, R.string.MYR, R.drawable.flag_malaysia)
    class NOK(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.NOK, value, value, R.string.NOK, R.drawable.flag_norwey)
    class NZD(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.NZD, value, value, R.string.NZD, R.drawable.flag_new_zealand)
    class PHP(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.PHP, value, value, R.string.PHP, R.drawable.flag_phillipines)
    class PLN(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.PLN, value, value, R.string.PLN, R.drawable.flag_poland)
    class RON(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.RON, value, value, R.string.RON, R.drawable.flag_romania)
    class RUB(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.RUB, value, value, R.string.RUB, R.drawable.flag_russia)
    class SEK(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.SEK, value, value, R.string.SEK, R.drawable.flag_sweeden)
    class SGD(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.SGD, value, value, R.string.SGD, R.drawable.flag_singapour)
    class THB(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.THB, value, value, R.string.THB, R.drawable.flag_thailand)
    class TRY(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.TRY, value, value, R.string.TRY, R.drawable.flag_turkey)
    class USD(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.USD, value, value, R.string.USD, R.drawable.flag_us)
    class ZAR(value: BigDecimal) : ExchangeRateViewModel(CurrencyConstants.ZAR, value, value, R.string.ZAR, R.drawable.flag_rsa)

}