package com.example.currency.presentation

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.currency.presentation.model.ExchangeRateViewModel
import java.math.BigDecimal
import javax.inject.Inject


class ExchangeRatesAdapter @Inject constructor() : RecyclerView.Adapter<ExchangeRateViewHolder>() {

    var items = mutableListOf<ExchangeRateViewModel>()
        set(value) {
            field = buildList(field, value)
            notifyItemRangeChanged(1, field.size)
        }

    var onItemClick: (String) -> Unit = {}

    private var multiplier: BigDecimal = BigDecimal.ONE
    private var enableBindingData = false

    private fun buildList(
        oldList: MutableList<ExchangeRateViewModel>,
        newList: MutableList<ExchangeRateViewModel>
    ): MutableList<ExchangeRateViewModel> {
        return if (oldList.isEmpty()) {
            newList[0].multipliedValue = BigDecimal.ONE
            newList
        } else {
            for (i in oldList.indices) {
                if (i != 0) {
                    oldList[i] = newList.first { listItem -> oldList[i].shortName == listItem.shortName }
                    oldList[i].multipliedValue = oldList[i].exchangeRate.multiply(multiplier)
                }
            }
            oldList
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangeRateViewHolder {
        val inflated = LayoutInflater.from(parent.context).inflate(ExchangeRateViewHolder.LAYOUT_ID, parent, false)
        return ExchangeRateViewHolder(inflated, ::onItemClick, ::onMultiplierChange)
    }

    override fun onBindViewHolder(holder: ExchangeRateViewHolder, position: Int) {
        enableBindingData = true
        holder.bind(items[position])
        enableBindingData = false
    }

    private fun onMultiplierChange(multiplier: BigDecimal) {
        this.multiplier = multiplier
        for (i in items.indices) {
            if (i == 0) {
                items[i].multipliedValue = multiplier
            } else {
                items[i].multipliedValue = items[i].exchangeRate.multiply(multiplier)
            }
        }
        updateHolderItems()
    }

    private fun updateHolderItems() {
        if (!enableBindingData) {
            notifyItemRangeChanged(1, itemCount)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun onItemClick(viewModel: ExchangeRateViewModel, position: Int) {
        multiplier = viewModel.multipliedValue
        onItemClick.invoke(items[position].shortName)
        moveItemToTop(position)
    }

    private fun moveItemToTop(position: Int) {
        position.takeIf { it > 0 }?.also { currentPosition ->
            items.removeAt(currentPosition).also {
                items.add(0, it)
            }
            notifyItemMoved(currentPosition, 0)
        }
    }
}