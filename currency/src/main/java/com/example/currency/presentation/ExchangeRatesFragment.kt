package com.example.currency.presentation

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.core.presentation.BaseFragment
import com.example.currency.R
import com.example.currency.presentation.model.ExchangeRateViewModel
import kotlinx.android.synthetic.main.exchange_rates_fragment.*
import javax.inject.Inject


class ExchangeRatesFragment : BaseFragment<ExchangeRatesContract.Presenter>(), ExchangeRatesContract.View {

    companion object {
        fun newInstance() = ExchangeRatesFragment()

        const val PARAM_BASE_CURRENCY = "BASE_CURRENCY"
    }

    @Inject
    override lateinit var presenter: ExchangeRatesContract.Presenter

    @Inject
    lateinit var adapter: ExchangeRatesAdapter


    override fun getLayoutResource() = R.layout.exchange_rates_fragment

    override fun getIdent() = "ExchangeRates"

    override fun initView() {
        rvExchangeRates.layoutManager = LinearLayoutManager(requireContext())
        rvExchangeRates.adapter = adapter

        adapter.onItemClick = {
            scrollToTop()
            presenter.onCurrencyClicked(it)
        }
    }

    private fun scrollToTop() {
        (rvExchangeRates.layoutManager as LinearLayoutManager).scrollToPosition(0)
    }

    override fun showExchangeRates(data: List<ExchangeRateViewModel>) {
        adapter.items = data.toMutableList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(PARAM_BASE_CURRENCY, presenter.onCurrencyStateSaved())
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let {
            presenter.onCurrencyStateRestored(it.getString(PARAM_BASE_CURRENCY))
        }
    }
}