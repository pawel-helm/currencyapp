package com.example.currency.presentation

import android.support.v7.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.view.View
import com.example.core.presentation.utils.SimpleTextWatcher
import com.example.currency.R
import com.example.currency.presentation.model.ExchangeRateViewModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.exchange_rate_item.*
import java.math.BigDecimal
import java.math.RoundingMode


class ExchangeRateViewHolder(
    override val containerView: View,
    private val onClick: (ExchangeRateViewModel, Int) -> Unit,
    private val onMultiplierChange: (BigDecimal) -> Unit
) : RecyclerView.ViewHolder(containerView),
    LayoutContainer {

    companion object {
        val LAYOUT_ID = R.layout.exchange_rate_item
        val DEFAULT_EMPTY_VALUE = BigDecimal.ZERO
    }

    init {
        etExchangeRate.addTextChangedListener(SimpleTextWatcher({
            if (adapterPosition == 0) {
                if (it.isEmpty()) {
                    onMultiplierChange.invoke(DEFAULT_EMPTY_VALUE)
                } else {
                    onMultiplierChange.invoke(BigDecimal(it))
                }
            }
        }))
    }

    fun bind(viewModel: ExchangeRateViewModel) {
        ivFlag.setImageResource(viewModel.flagResourceId)
        tvCurrencyShortName.text = viewModel.shortName
        tvCurrencyFullName.text = containerView.resources.getText(viewModel.fullNameResourceId)

        etExchangeRate.text = SpannableStringBuilder(roundValue(viewModel.multipliedValue).toString())
        etExchangeRate.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                onClick.invoke(viewModel, adapterPosition)
            }
        }

        vgExchangeRateItem.setOnClickListener {
            onClick.invoke(viewModel, adapterPosition)
            etExchangeRate.requestFocus()
        }
    }

    private fun roundValue(value: BigDecimal): Double {
        return value.setScale(2, RoundingMode.CEILING).toDouble()
    }
}
