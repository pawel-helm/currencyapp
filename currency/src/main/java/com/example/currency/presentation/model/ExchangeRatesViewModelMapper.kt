package com.example.currency.presentation.model

import com.example.core.presentation.mapper.ViewMapper
import com.example.currency.domain.model.CurrencyConstants
import com.example.currency.domain.model.ExchangeRates
import java.math.BigDecimal
import javax.inject.Inject


class ExchangeRatesViewModelMapper @Inject constructor() : ViewMapper<List<ExchangeRateViewModel>, ExchangeRates> {


    override fun mapToView(type: ExchangeRates): MutableList<ExchangeRateViewModel> {
        return type.rates.map { currencyFactory(it.name, it.value) }
            .toMutableList()
            .setBaseCurrencyAtFirstPosition(type.base)
    }

    private fun MutableList<ExchangeRateViewModel>.setBaseCurrencyAtFirstPosition(baseCurrency: String): MutableList<ExchangeRateViewModel> {
        val itemToMove = first { it.shortName == baseCurrency }
        remove(itemToMove)
        add(0, itemToMove)
        return this
    }

    private fun currencyFactory(name: String, value: BigDecimal): ExchangeRateViewModel {
        return when (name) {
            CurrencyConstants.AUD -> ExchangeRateViewModel.AUD(value)
            CurrencyConstants.BGN -> ExchangeRateViewModel.BGN(value)
            CurrencyConstants.BRL -> ExchangeRateViewModel.BRL(value)
            CurrencyConstants.CAD -> ExchangeRateViewModel.CAD(value)
            CurrencyConstants.CHF -> ExchangeRateViewModel.CHF(value)
            CurrencyConstants.CNY -> ExchangeRateViewModel.CNY(value)
            CurrencyConstants.CZK -> ExchangeRateViewModel.CZK(value)
            CurrencyConstants.DKK -> ExchangeRateViewModel.DKK(value)
            CurrencyConstants.EUR -> ExchangeRateViewModel.EUR(value)
            CurrencyConstants.GBP -> ExchangeRateViewModel.GBP(value)
            CurrencyConstants.HKD -> ExchangeRateViewModel.HKD(value)
            CurrencyConstants.HRK -> ExchangeRateViewModel.HRK(value)
            CurrencyConstants.HUF -> ExchangeRateViewModel.HUF(value)
            CurrencyConstants.IDR -> ExchangeRateViewModel.IDR(value)
            CurrencyConstants.ILS -> ExchangeRateViewModel.ILS(value)
            CurrencyConstants.INR -> ExchangeRateViewModel.INR(value)
            CurrencyConstants.ISK -> ExchangeRateViewModel.ISK(value)
            CurrencyConstants.JPY -> ExchangeRateViewModel.JPY(value)
            CurrencyConstants.KRW -> ExchangeRateViewModel.KRW(value)
            CurrencyConstants.MXN -> ExchangeRateViewModel.MXN(value)
            CurrencyConstants.MYR -> ExchangeRateViewModel.MYR(value)
            CurrencyConstants.NOK -> ExchangeRateViewModel.NOK(value)
            CurrencyConstants.NZD -> ExchangeRateViewModel.NZD(value)
            CurrencyConstants.PHP -> ExchangeRateViewModel.PHP(value)
            CurrencyConstants.PLN -> ExchangeRateViewModel.PLN(value)
            CurrencyConstants.RON -> ExchangeRateViewModel.RON(value)
            CurrencyConstants.RUB -> ExchangeRateViewModel.RUB(value)
            CurrencyConstants.SEK -> ExchangeRateViewModel.SEK(value)
            CurrencyConstants.SGD -> ExchangeRateViewModel.SGD(value)
            CurrencyConstants.THB -> ExchangeRateViewModel.THB(value)
            CurrencyConstants.TRY -> ExchangeRateViewModel.TRY(value)
            CurrencyConstants.USD -> ExchangeRateViewModel.USD(value)
            CurrencyConstants.ZAR -> ExchangeRateViewModel.ZAR(value)
            else -> throw IllegalArgumentException("$name is not supported")
        }
    }


}


