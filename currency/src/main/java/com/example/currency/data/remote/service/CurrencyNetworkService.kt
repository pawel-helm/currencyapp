package com.example.currency.data.remote.service

import com.example.currency.data.remote.model.ExchangeRatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyNetworkService {

    @GET("latest")
    fun getLatestExchangeRates(@Query("base") baseCurrency: String): Single<ExchangeRatesResponse>

}