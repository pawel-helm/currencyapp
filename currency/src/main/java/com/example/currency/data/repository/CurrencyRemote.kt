package com.example.currency.data.repository

import com.example.currency.domain.model.ExchangeRates
import io.reactivex.Single

interface CurrencyRemote {
    fun getLatestExchangeRates(baseCurrency: String): Single<ExchangeRates>
}