package com.example.currency.data.remote.mapper

import com.example.core.data.mapper.RemoteMapper
import com.example.currency.domain.model.CurrencyConstants
import com.example.currency.data.remote.model.ExchangeRatesResponse
import com.example.currency.data.remote.model.RateItems
import com.example.currency.domain.model.ExchangeRate
import com.example.currency.domain.model.ExchangeRates
import java.math.BigDecimal

class ExchangeRatesResponseMapper : RemoteMapper<ExchangeRatesResponse, ExchangeRates> {

    override fun mapFromRemote(response: ExchangeRatesResponse): ExchangeRates {
        return ExchangeRates(
            base = response.base,
            rates = mapRates(response.rates)
        )
    }

    private fun mapRates(rateItems: RateItems): List<ExchangeRate> {
        return listOf(
            ExchangeRate(CurrencyConstants.AUD, BigDecimal.valueOf(rateItems.AUD)),
            ExchangeRate(CurrencyConstants.BGN, BigDecimal.valueOf(rateItems.BGN)),
            ExchangeRate(CurrencyConstants.BRL, BigDecimal.valueOf(rateItems.BRL)),
            ExchangeRate(CurrencyConstants.CAD, BigDecimal.valueOf(rateItems.CAD)),
            ExchangeRate(CurrencyConstants.CHF, BigDecimal.valueOf(rateItems.CHF)),
            ExchangeRate(CurrencyConstants.CNY, BigDecimal.valueOf(rateItems.CNY)),
            ExchangeRate(CurrencyConstants.CZK, BigDecimal.valueOf(rateItems.CZK)),
            ExchangeRate(CurrencyConstants.DKK, BigDecimal.valueOf(rateItems.DKK)),
            ExchangeRate(CurrencyConstants.EUR, BigDecimal.valueOf(rateItems.EUR)),
            ExchangeRate(CurrencyConstants.GBP, BigDecimal.valueOf(rateItems.GBP)),
            ExchangeRate(CurrencyConstants.HKD, BigDecimal.valueOf(rateItems.HKD)),
            ExchangeRate(CurrencyConstants.HRK, BigDecimal.valueOf(rateItems.HRK)),
            ExchangeRate(CurrencyConstants.HUF, BigDecimal.valueOf(rateItems.HUF)),
            ExchangeRate(CurrencyConstants.IDR, BigDecimal.valueOf(rateItems.IDR)),
            ExchangeRate(CurrencyConstants.ILS, BigDecimal.valueOf(rateItems.ILS)),
            ExchangeRate(CurrencyConstants.INR, BigDecimal.valueOf(rateItems.INR)),
            ExchangeRate(CurrencyConstants.ISK, BigDecimal.valueOf(rateItems.ISK)),
            ExchangeRate(CurrencyConstants.JPY, BigDecimal.valueOf(rateItems.JPY)),
            ExchangeRate(CurrencyConstants.KRW, BigDecimal.valueOf(rateItems.KRW)),
            ExchangeRate(CurrencyConstants.MXN, BigDecimal.valueOf(rateItems.MXN)),
            ExchangeRate(CurrencyConstants.MYR, BigDecimal.valueOf(rateItems.MYR)),
            ExchangeRate(CurrencyConstants.NOK, BigDecimal.valueOf(rateItems.NOK)),
            ExchangeRate(CurrencyConstants.NZD, BigDecimal.valueOf(rateItems.NZD)),
            ExchangeRate(CurrencyConstants.PHP, BigDecimal.valueOf(rateItems.PHP)),
            ExchangeRate(CurrencyConstants.PLN, BigDecimal.valueOf(rateItems.PLN)),
            ExchangeRate(CurrencyConstants.RON, BigDecimal.valueOf(rateItems.RON)),
            ExchangeRate(CurrencyConstants.RUB, BigDecimal.valueOf(rateItems.RUB)),
            ExchangeRate(CurrencyConstants.SEK, BigDecimal.valueOf(rateItems.SEK)),
            ExchangeRate(CurrencyConstants.SGD, BigDecimal.valueOf(rateItems.SGD)),
            ExchangeRate(CurrencyConstants.THB, BigDecimal.valueOf(rateItems.THB)),
            ExchangeRate(CurrencyConstants.TRY, BigDecimal.valueOf(rateItems.TRY)),
            ExchangeRate(CurrencyConstants.USD, BigDecimal.valueOf(rateItems.USD)),
            ExchangeRate(CurrencyConstants.ZAR, BigDecimal.valueOf(rateItems.ZAR))
        )
    }
}
