package com.example.currency.data

import com.example.currency.data.repository.CurrencyRemote
import com.example.currency.domain.model.ExchangeRates
import com.example.currency.domain.repository.CurrencyRepository
import io.reactivex.Single
import javax.inject.Inject


class CurrencyDataRepository @Inject constructor(private val currencyRemote: CurrencyRemote) : CurrencyRepository {

    override fun getLatestExchangeRates(baseCurrency: String): Single<ExchangeRates> =
        currencyRemote.getLatestExchangeRates(baseCurrency)


}