package com.example.currency.data.remote

import com.example.currency.data.remote.mapper.ExchangeRatesResponseMapper
import com.example.currency.data.remote.service.CurrencyNetworkService
import com.example.currency.data.repository.CurrencyRemote
import com.example.currency.domain.model.ExchangeRates
import io.reactivex.Single
import javax.inject.Inject


class CurrencyRemoteImpl @Inject constructor(
    private val networkService: CurrencyNetworkService,
    private val exchangeRatesResponseMapper: ExchangeRatesResponseMapper
) : CurrencyRemote {

    override fun getLatestExchangeRates(baseCurrency: String): Single<ExchangeRates>
            = networkService.getLatestExchangeRates(baseCurrency).map { exchangeRatesResponseMapper.mapFromRemote(it) }

}