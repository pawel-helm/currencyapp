package com.example.currency.injection

import com.example.core.injection.scopes.PerApplication
import com.example.currency.data.CurrencyDataRepository
import com.example.currency.data.remote.CurrencyRemoteImpl
import com.example.currency.data.remote.mapper.ExchangeRatesResponseMapper
import com.example.currency.data.remote.service.CurrencyNetworkService
import com.example.currency.data.repository.CurrencyRemote
import com.example.currency.domain.repository.CurrencyRepository
import dagger.Module
import dagger.Provides


@Module
open class CurrencyModule {

    @Provides
    @PerApplication
    fun provideExchnageRatesResponseMapper() = ExchangeRatesResponseMapper()

    @Provides
    @PerApplication
    fun provideCardRemote(
        networkService: CurrencyNetworkService,
        exchangeRatesResponseMapper: ExchangeRatesResponseMapper
    ): CurrencyRemote = CurrencyRemoteImpl(networkService,
        exchangeRatesResponseMapper)

    @Provides
    @PerApplication
    fun provideCurrencyRepository(currencyRemote: CurrencyRemote): CurrencyRepository = CurrencyDataRepository(currencyRemote)


}