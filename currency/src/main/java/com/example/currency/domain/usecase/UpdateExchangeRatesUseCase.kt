package com.example.currency.domain.usecase

import com.example.core.domain.executor.PostExecutionThread
import com.example.core.domain.executor.ThreadExecutor
import com.example.core.domain.usecase.ObservableUseCase
import com.example.currency.domain.model.ExchangeRates
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class UpdateExchangeRatesUseCase @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    private val getLatestExchangeRatesUseCase: GetLatestExchangeRatesUseCase
) : ObservableUseCase<ExchangeRates, String>(threadExecutor, postExecutionThread) {

    companion object {
        const val INITIAL_DELAY = 0L
        const val UPDATE_TIME_INTERVAL = 1L
    }

    override fun createObservableUseCase(params: String?): Observable<ExchangeRates> {
        return Observable.interval(INITIAL_DELAY, UPDATE_TIME_INTERVAL, TimeUnit.SECONDS).flatMap {
            getLatestExchangeRatesUseCase.createSingleUseCase(params).toObservable()
        }
    }

}