package com.example.currency.domain.model

import java.math.BigDecimal


data class ExchangeRates(
    val base: String,
    val rates: List<ExchangeRate>
)

data class ExchangeRate(
    val name: String,
    val value: BigDecimal
)