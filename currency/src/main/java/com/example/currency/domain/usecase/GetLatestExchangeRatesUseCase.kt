package com.example.currency.domain.usecase

import com.example.core.domain.executor.PostExecutionThread
import com.example.core.domain.executor.ThreadExecutor
import com.example.core.domain.usecase.SingleUseCase
import com.example.currency.domain.model.CurrencyConstants
import com.example.currency.domain.model.ExchangeRates
import com.example.currency.domain.repository.CurrencyRepository
import io.reactivex.Single
import javax.inject.Inject


class GetLatestExchangeRatesUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                        postExecutionThread: PostExecutionThread,
                                                        private val currencyRepository: CurrencyRepository)
    : SingleUseCase<ExchangeRates, String>(threadExecutor, postExecutionThread) {


    companion object {
        const val DEFAULT_BASE_CURRENCY = CurrencyConstants.EUR
    }

    override fun createSingleUseCase(params: String?): Single<ExchangeRates> {
        return currencyRepository.getLatestExchangeRates(params ?: DEFAULT_BASE_CURRENCY)
    }

}
