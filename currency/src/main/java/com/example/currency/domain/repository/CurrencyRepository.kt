package com.example.currency.domain.repository

import com.example.currency.domain.model.ExchangeRates
import io.reactivex.Single

interface CurrencyRepository {

    fun getLatestExchangeRates(baseCurrency: String): Single<ExchangeRates>

}