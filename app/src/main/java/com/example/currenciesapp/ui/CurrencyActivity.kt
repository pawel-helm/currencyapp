package com.example.currenciesapp.ui

import android.os.Bundle
import com.example.core.presentation.BaseActivity
import com.example.currenciesapp.utils.Navigator
import com.example.currenciesapp.R
import javax.inject.Inject

class CurrencyActivity : BaseActivity() {

    @Inject
    lateinit var navigator: Navigator

    override fun getLayoutResource() = R.layout.base_activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigator.showExchangeRatesFragment(this, false)
    }
}