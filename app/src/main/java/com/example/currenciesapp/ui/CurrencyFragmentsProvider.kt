package com.example.currenciesapp.ui

import com.example.currency.presentation.ExchangeRatesFragment
import com.example.currency.presentation.ExchangeRatesFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class CurrencyFragmentsProvider {

    @ContributesAndroidInjector(modules = arrayOf(ExchangeRatesFragmentModule::class))
    abstract fun provideStartFragmentFactory(): ExchangeRatesFragment
}