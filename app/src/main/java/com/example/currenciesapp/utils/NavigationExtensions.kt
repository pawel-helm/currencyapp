package com.example.currenciesapp.utils

import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import com.example.core.presentation.BaseContract
import com.example.core.presentation.BaseFragment
import com.example.currenciesapp.R


fun FragmentActivity.showFragment(
    fragment: BaseFragment<*>,
    resetStack: Boolean = false,
    forceSameFragment: Boolean = false
) {

    if (!forceSameFragment) {
        if (displayFragmentFromStackIfPresent(fragment.getIdent())) {
            return
        }
    }

    if (resetStack) {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
    fragmentTransaction.replace(R.id.vgBaseFragmentContainer, fragment, fragment.getIdent())
    fragmentTransaction.addToBackStack(fragment.getIdent())

    if (!isFinishing) {
        fragmentTransaction.commit()
    }
}

fun FragmentActivity.displayFragmentFromStackIfPresent(ident: String): Boolean {
    if (supportFragmentManager.backStackEntryCount > 0) {
        for (i in 0 until supportFragmentManager.backStackEntryCount) {
            val fragmentName = supportFragmentManager.getBackStackEntryAt(i).name
            val fragment = supportFragmentManager.findFragmentByTag(fragmentName)
            if ((fragment as BaseContract.View).getIdent() == ident) {
                supportFragmentManager.popBackStack(ident, 0)
                return true
            }
        }
    }
    return false
}



