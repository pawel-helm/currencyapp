package com.example.currenciesapp.utils

import com.example.core.presentation.BaseActivity
import com.example.currenciesapp.utils.showFragment
import com.example.currency.presentation.ExchangeRatesFragment

class Navigator {

    fun showExchangeRatesFragment(activity: BaseActivity, resetStack: Boolean = true) {
        activity.showFragment(ExchangeRatesFragment.newInstance(), resetStack)
    }

}