package com.example.currenciesapp.injection

import com.example.currenciesapp.ui.CurrencyActivity
import com.example.currenciesapp.ui.CurrencyFragmentsProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [CurrencyFragmentsProvider::class])
    abstract fun bindCurrencyActivity(): CurrencyActivity

}