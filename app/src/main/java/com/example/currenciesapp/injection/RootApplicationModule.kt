package com.example.currenciesapp.injection

import android.app.Application
import android.content.Context
import com.example.core.data.executor.JobExecutor
import com.example.core.data.remote.NetworkServiceFactory
import com.example.core.domain.executor.PostExecutionThread
import com.example.core.domain.executor.ThreadExecutor
import com.example.core.injection.scopes.PerApplication
import com.example.core.presentation.UIThread
import com.example.currenciesapp.BuildConfig
import com.example.currenciesapp.utils.Navigator
import com.example.currency.data.remote.service.CurrencyNetworkService
import dagger.Module
import dagger.Provides

@Module
open class RootApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @PerApplication
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @PerApplication
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @PerApplication
    fun provideNavigator(): Navigator {
        return Navigator()
    }

    @Provides
    @PerApplication
    fun provideCurrencyNetworkService(networkServiceFactory: NetworkServiceFactory): CurrencyNetworkService {
        return networkServiceFactory.makeRetrofitService(BuildConfig.URL_WEB_SERVICE)
            .create(CurrencyNetworkService::class.java)
    }

}