package com.example.currenciesapp.injection

import android.app.Application
import com.example.core.data.remote.NetworkModule
import com.example.core.injection.scopes.PerApplication
import com.example.currenciesapp.RootApplication
import com.example.currency.injection.CurrencyModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(modules = [
    (AndroidSupportInjectionModule::class),
    (NetworkModule::class),
    (RootApplicationModule::class),
    (CurrencyModule::class),
    (ActivityBuilder::class)])

interface RootApplicationComponent : AndroidInjector<DaggerApplication> {


    fun inject(rootApp: RootApplication)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): RootApplicationComponent
    }

}