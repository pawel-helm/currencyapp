package com.example.currenciesapp

import com.example.currenciesapp.injection.DaggerRootApplicationComponent
import com.github.ajalt.timberkt.Timber
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


class RootApplication: DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerRootApplicationComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }


    override fun onCreate() {
        super.onCreate()
        Timber.plant(timber.log.Timber.DebugTree())
    }
}