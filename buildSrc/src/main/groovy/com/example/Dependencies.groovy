package com.example

 
class Dependencies {

    private static class Versions {
        static final gradle_version = "3.2.1"
        static final kotlin_version = "1.3.10"
        static final constraint_layout_version = "1.1.2"
        static final dagger_version = "2.16"
        static final rxjava2_version = '2.1.14'
        static final rxandroid2_version = '2.0.2'
        static final retrofit_version = "2.4.0"
        static final gson_version = "2.8.5"
        static final okhttp_version = "3.10.0"
        static final timberkt_version = "1.5.0"
        static final support_version = "28.0.0"
        static final room_version = "1.1.1"
        static final crashlytics_version = "2.9.4"
        static final fabric_gradle_version = "1.25.4"
        static final anko_version = "0.10.5"
        static final rxpermissions_version = "0.10.2"
        static final androidktx_version = "0.3"
        static final material_dialog_version = "0.9.6.0"
        static final leakcanary_version = "1.6.1"
        static final stetho_version = "1.5.0"
        static final picasso_version = "2.71828"
        static final joda_time_version = "2.10.0"
        static final paging_runtime_version = "1.0.1"
        static final paging_rx_version = "1.0.0-rc1"
        static final rootbeer_version = "0.0.7"
        static final framgia_emulator_detector_version = "1.4.0"
        static final page_indicator_view_version = "1.0.2"

        /* Test frameworks */
        static final junit_version = "4.12"
        static final test_runner_version = "1.0.2"
        static final espresso_core_version = "3.0.2"
        static final mockito_kotlin_version = "2.0.0"
        static final junit5_version = "5.3.1"
        static final mockk_version = "1.8.13"
        static final android_junit5 = "1.3.1.1"
    }

    /* Gradle */
    static final gradle = "com.android.tools.build:gradle:$Versions.gradle_version"

    /* Support */
    static final support_app_compat_v7 = "com.android.support:appcompat-v7:$Versions.support_version"
    static final support_dynamic_animation = "com.android.support:support-dynamic-animation:$Versions.support_version"
    static final support_recycler_view = "com.android.support:recyclerview-v7:$Versions.support_version"
    static final support_design = "com.android.support:design:$Versions.support_version"
    static final support_card_view = "com.android.support:cardview-v7:$Versions.support_version"

    /* Kotlin */
    static final kotlin_stdlib_jre7 = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$Versions.kotlin_version"
    static final kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$Versions.kotlin_version"

    /* Constraint layout */
    static final constraint_layout = "com.android.support.constraint:constraint-layout:$Versions.constraint_layout_version"

    /* Dagger */
    static final dagger = "com.google.dagger:dagger:"
    static final dagger_android = "com.google.dagger:dagger-android:$Versions.dagger_version"
    static final dagger_android_support = "com.google.dagger:dagger-android-support:$Versions.dagger_version"
    static final dagger_compiler = "com.google.dagger:dagger-compiler:$Versions.dagger_version"
    static final dagger_android_processor = "com.google.dagger:dagger-android-processor:$Versions.dagger_version"

    /* Rx */
    static final rxandroid = "io.reactivex.rxjava2:rxandroid:$Versions.rxandroid2_version"
    static final rxjava = "io.reactivex.rxjava2:rxjava:$Versions.rxjava2_version"

    /* RxPermissions */
    static final rxpermissions = "com.github.tbruyelle:rxpermissions:$Versions.rxpermissions_version"

    /* Jake Wharton's Timber Kotlin impl */
    static final timberkt = "com.github.ajalt:timberkt:$Versions.timberkt_version"

    /* Retrofit */
    static final retrofit = "com.squareup.retrofit2:retrofit:$Versions.retrofit_version"
    static final adapter_rxjava2 = "com.squareup.retrofit2:adapter-rxjava2:$Versions.retrofit_version"
    static final converter_gson = "com.squareup.retrofit2:converter-gson:$Versions.retrofit_version"

    /* Gson */
    static final gson = "com.google.code.gson:gson:$Versions.gson_version"

    /* OkHttp */
    static final okhttp = "com.squareup.okhttp3:okhttp:$Versions.okhttp_version"
    static final okhttp_logging_interceptor = "com.squareup.okhttp3:logging-interceptor:$Versions.okhttp_version"

    /* Room */
    static final room_runtime = "android.arch.persistence.room:runtime:$Versions.room_version"
    static final room_compiler = "android.arch.persistence.room:compiler:$Versions.room_version"

    /* Crashlytics */
    static final crashlytics = "com.crashlytics.sdk.android:crashlytics:$Versions.crashlytics_version@aar"
    static final fabric_gradle = "io.fabric.tools:gradle:$Versions.fabric_gradle_version"

    /* Test Dependencies */
    static final kotlin_test = "org.jetbrains.kotlin:kotlin-test-junit:$Versions.kotlin_version"
    static final junit = "junit:junit:$Versions.junit_version"
    static final test_runner = "com.android.support.test:runner:$Versions.test_runner_version"
    static final espresso_core = "com.android.support.test.espresso:espresso-core:$Versions.espresso_core_version"
    static final mockito_kotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:$Versions.mockito_kotlin_version"

    static final junit5_jupiter = "org.junit.jupiter:junit-jupiter-api:$Versions.junit5_version"
    static final junit5_engine = "org.junit.jupiter:junit-jupiter-engine:$Versions.junit5_version"
    static final android_junit5 = "de.mannodermaus.gradle.plugins:android-junit5:$Versions.android_junit5"
    static final mockk = "io.mockk:mockk:$Versions.mockk_version"

    /* Anko */
    static final anko_common = "org.jetbrains.anko:anko-common:$Versions.anko_version"
    static final anko_support =  "org.jetbrains.anko:anko-support-v4:$Versions.anko_version"

    /* Android KTX */
    static final android_ktx = "androidx.core:core-ktx:$Versions.androidktx_version"

    /* Material Dialogs */
    static final material_dialog = "com.afollestad.material-dialogs:commons:$Versions.material_dialog_version"

    /* LeakCanary */
    static final leakcanary = "com.squareup.leakcanary:leakcanary-android:$Versions.leakcanary_version"
    static final leakcanary_no_op = "com.squareup.leakcanary:leakcanary-android-no-op:$Versions.leakcanary_version"
    static final leakcanary_support = "com.squareup.leakcanary:leakcanary-support-fragment:$Versions.leakcanary_version"

    /* Picasso */
    static final picasso = "com.squareup.picasso:picasso:$Versions.picasso_version"

    /* Stetho */
    static final stetho = "com.facebook.stetho:stetho:$Versions.stetho_version"

    /* Jada Time */
    static final joda_time = "net.danlew:android.joda:$Versions.joda_time_version"

    /* Paging */
    static final paging_runtime = "android.arch.paging:runtime:$Versions.paging_runtime_version"
    static final paging_rx = "android.arch.paging:rxjava2:$Versions.paging_rx_version"

    /* RootBeer */
    static final rootbeer  = "com.scottyab:rootbeer-lib:$Versions.rootbeer_version"

    /* Emulator detector */
    static final emulator_detector = "com.github.framgia:android-emulator-detector:$Versions.framgia_emulator_detector_version"

    /* View pager */
    static final view_pager_indicator = "com.romandanylyk:pageindicatorview:$Versions.page_indicator_view_version"

}