package com.example.core.data.remote

import com.example.core.injection.scopes.PerApplication
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    @PerApplication
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @PerApplication
    fun provideConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

    @Provides
    @PerApplication
    fun provideOkHttpClient(okHttpClientProvider: OkHttpClientProvider): OkHttpClient = okHttpClientProvider.provide()


}