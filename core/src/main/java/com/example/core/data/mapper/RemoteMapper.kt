package com.example.core.data.mapper

interface RemoteMapper<in M, out E> {

    fun mapFromRemote(response: M): E

}