package com.example.core.injection.scopes

import javax.inject.Scope

const val URL_WEB_SERVICE = "URL_WEB_SERVICE"

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication