package com.example.core.domain.usecase

import com.example.core.domain.executor.PostExecutionThread
import com.example.core.domain.executor.ThreadExecutor
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


abstract class SingleUseCase<T, in Params>(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) {

    /**
     * Builds a [Single] which will be used when the current [SingleUseCase] is executed.
     */
    abstract fun createSingleUseCase(params: Params? = null): Single<T>

    /**
     * Executes the current use case. Should be used only in Presenters
     */
    fun execute(subscriber: Subscriber<T>, params: Params? = null): Disposable {
        return this.createSingleUseCase(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.scheduler)
            .doOnSubscribe { subscriber.doOnSubscribe() }
            .doFinally { subscriber.doFinally() }
            .subscribe(subscriber::onSuccess, subscriber::onError)
    }

    abstract class Subscriber<T> {
        abstract fun onSuccess(result: T)
        abstract fun onError(exp: Throwable)
        open fun doOnSubscribe() {}
        open fun doFinally() {}
    }
}