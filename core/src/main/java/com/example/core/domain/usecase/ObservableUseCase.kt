package com.example.core.domain.usecase

import com.example.core.domain.executor.PostExecutionThread
import com.example.core.domain.executor.ThreadExecutor
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


abstract class ObservableUseCase<T, in Params>(
    private val threadExecutor: ThreadExecutor,
    private val postExecutionThread: PostExecutionThread
) {

    /**
     * Builds a [Observable] which will be used when the current [ObservableUseCase] is executed.
     */
    abstract fun createObservableUseCase(params: Params? = null): Observable<T>

    /**
     * Executes the current use case. Should be used only in Presenters
     */
    fun execute(subscriber: Subscriber<T>, params: Params? = null): Disposable {
        return this.createObservableUseCase(params)
            .subscribeOn(Schedulers.from(threadExecutor))
            .observeOn(postExecutionThread.scheduler)
            .doOnSubscribe { subscriber.doOnSubscribe() }
            .doFinally { subscriber.doFinally() }
            .subscribe(subscriber::onSuccess, subscriber::onError)
    }

    abstract class Subscriber<T> {
        abstract fun onSuccess(result: T)
        abstract fun onError(exp: Throwable)
        open fun doOnSubscribe() {}
        open fun doFinally() {}
    }
}