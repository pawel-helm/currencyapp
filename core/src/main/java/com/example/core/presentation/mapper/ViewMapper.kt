package com.example.core.presentation.mapper


interface ViewMapper<out V, in D> {

    fun mapToView(type: D): V

}