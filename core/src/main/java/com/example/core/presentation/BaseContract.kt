package com.example.core.presentation

interface BaseContract {

    interface Presenter {
        fun init()
        fun start()
        fun resume()
        fun pause()
        fun stop()
        fun destroyView()
        fun destroy()
    }

    interface View {
        fun getLayoutResource(): Int
        fun getIdent(): String
    }

}