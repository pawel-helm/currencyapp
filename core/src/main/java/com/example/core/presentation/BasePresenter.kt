package com.example.core.presentation

import android.support.annotation.CallSuper
import io.reactivex.disposables.Disposable

abstract class BasePresenter : BaseContract.Presenter {

    private val weakDisposablesMap = HashMap<String, Disposable>()
    private val persistDisposableMap = HashMap<String, Disposable>()

    @CallSuper
    override fun init() {
    }

    @CallSuper
    override fun start() {
    }

    @CallSuper
    override fun resume() {
    }

    @CallSuper
    override fun pause() {
    }

    @CallSuper
    override fun stop() {
        dispose(weakDisposablesMap)
    }

    @CallSuper
    override fun destroyView() {
        dispose(persistDisposableMap)
    }

    @CallSuper
    override fun destroy() {
    }

    protected fun Disposable.addWeakDisposable(tag: String = "DEFAULT") {
        dispose(weakDisposablesMap, tag)
        weakDisposablesMap[tag] = this
    }

    protected fun Disposable.addPersistDisposable(tag: String = "DEFAULT") {
        dispose(persistDisposableMap, tag)
        persistDisposableMap[tag] = this
    }

    fun disposePersistDisposableByTag(tag: String) {
        val toRemove = persistDisposableMap.remove(tag)
        if (toRemove != null && !toRemove.isDisposed) {
            toRemove.dispose()
        }
    }

    private fun dispose(disposablesMap: HashMap<String, Disposable>, tag: String? = null) {
        if (tag == null) {
            disposablesMap.forEach { if (!it.value.isDisposed) it.value.dispose() }
            disposablesMap.clear()
        } else {
            val previous = disposablesMap.remove(tag)
            if (previous != null && !previous.isDisposed) {
                previous.dispose()
            }
        }
    }
}